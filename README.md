# microfsm

A very small (<~200 lines), generic FSM implementation in C++

![microFSM](docs/microfsm.png "microFSM")

## Requirements

- A C++11 compiler.
- Maybe [meson](https://mesonbuild.com/), for building tests, etc.

## Features

- Any (less-than comparable) type as a state type.
- Any callable as a transition function.
- Enforces valid end states at run-time.
- The FSM design is non-deterministic.

## Usage

```cpp

enum state { START, MIDDLE, END };

// Create an instance specifying:
// - potential argument types for the progress and transition
//   functions
// - a start state
// - one or more end states
microfsm<state, arg_type1, ...> fsm{ START, { END }};

// Add transition functions:
// - from a start state
// - with possible end states
fsm.add_transition(START,
    [](state, arg_type1) -> state {
      // Depending on argument, return MIDDLE or END
    },
    { MIDDLE, END }
);

// Progress from the current state, passing any specified
// arguments.
arg_type1 arg1;
fsm.progress(arg1, ...);

// Read the FSM state or meta state:
auto res = fsm.meta_state(); // one of RUNNING, ENDED, ERRORED
auto s = fsm.current_state(); // one of START, MIDDLE, END
auto e = fsm.error(); // if res == ERRORED
```
## License

Licensed under [MIT +no-false-attribs License](./LICENSE.txt)
