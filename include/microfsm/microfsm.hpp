/**
 * Copyright (C) Jens Finkhaeuser and other microfsm contributors.
 * Licensed under the MIT +no-false-attribs License. See LICENSE.txt for
 * licensing details.
 */
#ifndef MICROFSM_MICROFSM_HPP
#define MICROFSM_MICROFSM_HPP

#include <functional>
#include <set>
#include <map>

namespace microfsm {

// The FSM's internal meta-state; could be modelled via the state parameter,
// but the code becomes easier if it's separated.
enum meta_state_t
{
  RUNNING = 0,  // The FSM is ready to transition from one state to another
  ERRORED = 1,  // The FSM has encountered an error and can't transition
  ENDED = 2,    // The FSM has reached one of the defined end states.
};

// Error conditions for meta_state_t == ERRORED.
enum error_condition_t
{
  NONE                 = 0, // No errors
  NO_KNOWN_TRANSITION  = 1, // There is no known transition from the current
                            // state.
  TRANSITION_ERROR     = 2, // The transition function errored.
  INVALID_RESULT_STATE = 3, // The transition function returned a state that
                            // was not specified as one of the permissible
                            // results.
};


/**
 * FSM template takes two or more arguments:
 * - stateT, the type that holds the FSM state(s). Typically an enum, but could
 *   be an int, etc. We assign and copy it, and we less compare it, so it needs
 *   to have those properties.
 * - START_STATE, a value of type stateT that is the state with which the FSM
 *   gets initialized.
 * - Any other types specify which arguments are expected to be passed to
 *   state transition functions.
 *
 * The general design of the FSM class assumes that for any given state, there
 * is exactly one transition function. Said transition function does whatever
 * it needs to do, and returns a result state. The FSM merely checks that the
 * result state is valid before assuming the state.
 *
 * The FSM constructor takes a list of end states. If an end state is reached,
 * the FSM can no longer progress. Similarly, if one of a set of error
 * conditions is met, the FSM no longer can progress.
 *
 * Usage Note:
 *   There is one fairly important design contraint: from within a transition
 *   function, you cannot reliably progress the FSM again. That is, your code
 *   must provide it's own run loop; the FSM will not run automatically until
 *   ended:
 *
 *   microfsm::fsm<...> fsm{...};
 *
 *   while (fsm.meta_state() == microfsm::RUNNING) {
 *     fsm.progress(...);
 *   }
 */
template <
  typename stateT,  // State type for the FSM
  typename... argsT // Transition function/progress arguments
>
struct fsm
{
  // A set of states
  typedef std::set<stateT> state_set_t;

  // The state transition function takes the current state, followed by zero or
  // more arguments of the type passed to the fsm template.
  typedef std::function<stateT (stateT const &, argsT...)> transition_function_t;

  /**
   * Construct the fsm with one or more end states.
   */
  fsm(stateT const & start_state, std::initializer_list<stateT> const & init)
    : m_start_state(start_state)
    , m_current_state(m_start_state)
    , m_end_states(init)
  {
    check_ended();
  }

  /**
   * Add a transition:
   * - from the state specified in the first argument
   * - via the transition function specified in the second argument
   * - to one of the states specified in the third argument (as an initializer
   *   list { foo, bar...} )
   *
   * May throw logic_error if more than one transition function is to be
   * registered for a state.
   */
  void add_transition(stateT const & start_state,
      transition_function_t && transition,
      std::initializer_list<stateT> permissible_results)
  {
    if (m_state_transitions.find(start_state) != m_state_transitions.end()) {
      throw std::logic_error("Cannot register multiple transition functions "
          "for the same state.");
    }
    m_state_transitions[start_state] = {
      std::move(transition), // Avoids copy constructor, needs &&
      permissible_results
    };
  }

  /**
   * Progress the FSM from its current state to a new state. Pass any
   * arguments that the transition function is supposed to get.
   *
   * Returns one of RUNNING, ERRORED, ENDED.
   */
  meta_state_t progress(argsT... args)
  {
    // Make this a no-op if the FSM can't run.
    if (m_meta_state != RUNNING) {
      return m_meta_state;
    }

    // Find a transition
    auto transition = m_state_transitions.find(m_current_state);
    if (transition == m_state_transitions.end()) {
      m_error = NO_KNOWN_TRANSITION;
      m_meta_state = ERRORED;
      return m_meta_state;
    }

    // Execute transition
    stateT result = m_current_state;
    try {
      result = transition->second.transition_function(m_current_state,
          std::forward<argsT>(args)...);
    } catch (std::exception const &) {
      // XXX Maybe log exception?
      m_error = TRANSITION_ERROR;
      m_meta_state = ERRORED;
      return m_meta_state;
    } catch (...) {
      m_error = TRANSITION_ERROR;
      m_meta_state = ERRORED;
      return m_meta_state;
    }

    // Check result
    if (transition->second.permissible_results.find(result)
        == transition->second.permissible_results.end())
    {
      m_error = INVALID_RESULT_STATE;
      m_meta_state = ERRORED;
      return m_meta_state;
    }
    m_current_state = result;

    // Finally, report if this is an end state.
    check_ended();
    return m_meta_state;
  }

  /**
   * Reset the FSM for re-use. The transitions typically stay configured, but
   * the state gets reset.
   *
   * If callables or member functions were configured as transition functions,
   * those may hold their own internal state. If the optional flag is set, the
   * registered transition functions also get reset/deregistered, but then need
   * to be re-added.
   */
  void reset(bool reset_transitions = false)
  {
    m_current_state = m_start_state;
    m_meta_state = RUNNING;
    m_error = NONE;

    if (reset_transitions) {
      m_state_transitions.clear();
    }
  }


  // Accessors
  stateT current_state() const { return m_current_state; }
  meta_state_t meta_state() const { return m_meta_state; }
  error_condition_t error_condition() const { return m_error; }


private:
  void check_ended()
  {
    if (m_end_states.find(m_current_state) != m_end_states.end()) {
      m_meta_state = ENDED;
    }
  }

  // Transition information is the function and its permissible results.
  struct transition
  {
    transition_function_t transition_function;
    state_set_t permissible_results;
  };

  // We map from a current state to transition information
  typedef std::map<stateT, transition> state_transitions_t;


  state_transitions_t m_state_transitions = {};
  meta_state_t        m_meta_state = RUNNING;
  error_condition_t   m_error = NONE;

  stateT              m_start_state;
  stateT              m_current_state;
  state_set_t         m_end_states;
};

} // namespace microfsm

#endif // guard
