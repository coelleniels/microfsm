#include <gtest/gtest.h>

#include <microfsm/microfsm.hpp>

namespace {
  // A more complex FSM:
  // - from START, progress to LEFT or RIGHT
  // - from LEFT progress to RIGHT or END1
  // - from RIGHT progress to LEFT or END2
  //
  // Progress depends on int parameter.
  enum state {
    START, LEFT, RIGHT, END1, END2,
  };
  enum decisions {
    GO_RIGHT, GO_LEFT, GO_END1, GO_END2,
  };

  microfsm::fsm<state, int> get_fsm()
  {
    microfsm::fsm<state, int> fsm{ START, { END1, END2 } };

    fsm.add_transition(START,
        [](state, int dir) -> state {
          switch (dir) {
            case GO_LEFT:
              return LEFT;
            case GO_RIGHT:
              return RIGHT;
            case 666:
              // This is a very special case, in which we're throwing an error
              // that cannot be caught except with (...), because the error
              // struct is private. Hence the special case number.
              {
                struct evil {};
                throw evil{};
              }
            default:
              throw std::runtime_error("nope");
          }
        },
        { LEFT, RIGHT });

    fsm.add_transition(LEFT,
        [](state, int dir) -> state {
          switch (dir) {
            case GO_RIGHT:
              return RIGHT;
            case GO_END1:
              return END1;
            default:
              throw std::runtime_error("nope");
          }
        },
        { RIGHT, END1 });

    fsm.add_transition(RIGHT,
        [](state, int dir) -> state {
          switch (dir) {
            case GO_LEFT:
              return LEFT;
            case GO_END2:
              return END2;
            default:
              throw std::runtime_error("nope");
          }
        },
        { LEFT, END2 });

    return fsm;
  }
} // anonymous namespace

TEST(machines, transition_from_start_bad)
{
  auto fsm = get_fsm();

  // Anything but LEFT or RIGHT is an error
  EXPECT_EQ(fsm.meta_state(), microfsm::RUNNING);
  auto meta_state = fsm.progress(GO_END1);
  ASSERT_EQ(meta_state, microfsm::ERRORED);

  fsm.reset();
  EXPECT_EQ(fsm.meta_state(), microfsm::RUNNING);
  meta_state = fsm.progress(GO_END2);
  ASSERT_EQ(meta_state, microfsm::ERRORED);

  fsm.reset();
  EXPECT_EQ(fsm.meta_state(), microfsm::RUNNING);
  meta_state = fsm.progress(42);
  ASSERT_EQ(meta_state, microfsm::ERRORED);

  // Special evil case should also be caught.
  fsm.reset();
  EXPECT_EQ(fsm.meta_state(), microfsm::RUNNING);
  meta_state = fsm.progress(666);
  ASSERT_EQ(meta_state, microfsm::ERRORED);
}

TEST(machines, transition_from_start_good)
{
  auto fsm = get_fsm();

  EXPECT_EQ(START, fsm.current_state());
  fsm.progress(GO_LEFT);
  ASSERT_EQ(LEFT, fsm.current_state());

  fsm.reset();
  EXPECT_EQ(START, fsm.current_state());
  fsm.progress(GO_RIGHT);
  ASSERT_EQ(RIGHT, fsm.current_state());
}

TEST(machines, transition_from_start_via_left_to_end1)
{
  auto fsm = get_fsm();

  EXPECT_EQ(START, fsm.current_state());
  fsm.progress(GO_LEFT);
  ASSERT_EQ(LEFT, fsm.current_state());

  fsm.progress(GO_END1);
  ASSERT_EQ(END1, fsm.current_state());
  ASSERT_EQ(microfsm::ENDED, fsm.meta_state());
}

TEST(machines, reset_also_transitions)
{
  auto fsm = get_fsm();

  // First attempt works
  EXPECT_EQ(START, fsm.current_state());
  fsm.progress(GO_LEFT);
  ASSERT_EQ(LEFT, fsm.current_state());

  // Hard reset
  fsm.reset(true);

  // Second attempt fails to find transitions
  EXPECT_EQ(START, fsm.current_state());
  fsm.progress(GO_LEFT);
  ASSERT_EQ(microfsm::ERRORED, fsm.meta_state());
}
