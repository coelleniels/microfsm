#include <gtest/gtest.h>

#include <microfsm/microfsm.hpp>

TEST(progress, simple)
{
  enum states { FIRST, SECOND };
  microfsm::fsm<states> fsm{ FIRST, { SECOND } };

  EXPECT_EQ(fsm.current_state(), FIRST);
  EXPECT_EQ(fsm.meta_state(), microfsm::RUNNING);
  EXPECT_EQ(fsm.error_condition(), microfsm::NONE);

  fsm.add_transition(FIRST, [](states) -> states { return SECOND; }, { SECOND });

  EXPECT_EQ(fsm.current_state(), FIRST);
  EXPECT_EQ(fsm.meta_state(), microfsm::RUNNING);
  EXPECT_EQ(fsm.error_condition(), microfsm::NONE);

  fsm.progress();

  ASSERT_EQ(fsm.current_state(), SECOND);
  ASSERT_EQ(fsm.meta_state(), microfsm::ENDED);
  ASSERT_EQ(fsm.error_condition(), microfsm::NONE);

  fsm.reset();

  ASSERT_EQ(fsm.current_state(), FIRST);
  ASSERT_EQ(fsm.meta_state(), microfsm::RUNNING);
  ASSERT_EQ(fsm.error_condition(), microfsm::NONE);
}

TEST(progress, prevent_double_registration)
{
  enum states { FIRST, SECOND };
  microfsm::fsm<states> fsm{ FIRST, { SECOND } };

  EXPECT_EQ(fsm.current_state(), FIRST);
  EXPECT_EQ(fsm.meta_state(), microfsm::RUNNING);
  EXPECT_EQ(fsm.error_condition(), microfsm::NONE);

  fsm.add_transition(FIRST, [](states) -> states { return SECOND; }, { SECOND });

  EXPECT_EQ(fsm.current_state(), FIRST);
  EXPECT_EQ(fsm.meta_state(), microfsm::RUNNING);
  EXPECT_EQ(fsm.error_condition(), microfsm::NONE);

  ASSERT_THROW(fsm.add_transition(FIRST, [](states) -> states { return SECOND; }, { SECOND }),
      std::logic_error);

  // FSM can be used, nothing is lost
  ASSERT_EQ(fsm.current_state(), FIRST);
  ASSERT_EQ(fsm.meta_state(), microfsm::RUNNING);
  ASSERT_EQ(fsm.error_condition(), microfsm::NONE);
}

TEST(progress, no_registered_transition)
{
  enum states { FIRST, SECOND };
  microfsm::fsm<states> fsm{ FIRST, { SECOND } };

  EXPECT_EQ(fsm.current_state(), FIRST);
  EXPECT_EQ(fsm.meta_state(), microfsm::RUNNING);
  EXPECT_EQ(fsm.error_condition(), microfsm::NONE);

  auto meta_state = fsm.progress();

  ASSERT_EQ(meta_state, microfsm::ERRORED);
  ASSERT_EQ(fsm.meta_state(), microfsm::ERRORED);
  ASSERT_EQ(fsm.current_state(), FIRST);
  ASSERT_EQ(fsm.error_condition(), microfsm::NO_KNOWN_TRANSITION);
}

TEST(progress, transition_throws)
{
  enum states { FIRST, SECOND };
  microfsm::fsm<states> fsm{ FIRST, { SECOND } };

  EXPECT_EQ(fsm.current_state(), FIRST);
  EXPECT_EQ(fsm.meta_state(), microfsm::RUNNING);
  EXPECT_EQ(fsm.error_condition(), microfsm::NONE);

  fsm.add_transition(FIRST, [](states) -> states { throw std::runtime_error("test"); }, { SECOND });

  auto meta_state = fsm.progress();

  ASSERT_EQ(meta_state, microfsm::ERRORED);
  ASSERT_EQ(fsm.meta_state(), microfsm::ERRORED);
  ASSERT_EQ(fsm.current_state(), FIRST);
  ASSERT_EQ(fsm.error_condition(), microfsm::TRANSITION_ERROR);
}

TEST(progress, transition_returns_bad_state)
{
  enum states { FIRST, SECOND };
  microfsm::fsm<states> fsm{ FIRST, { SECOND } };

  EXPECT_EQ(fsm.current_state(), FIRST);
  EXPECT_EQ(fsm.meta_state(), microfsm::RUNNING);
  EXPECT_EQ(fsm.error_condition(), microfsm::NONE);

  fsm.add_transition(FIRST, [](states) -> states { return FIRST; }, { SECOND });

  auto meta_state = fsm.progress();

  ASSERT_EQ(meta_state, microfsm::ERRORED);
  ASSERT_EQ(fsm.meta_state(), microfsm::ERRORED);
  ASSERT_EQ(fsm.current_state(), FIRST);
  ASSERT_EQ(fsm.error_condition(), microfsm::INVALID_RESULT_STATE);
}

TEST(progress, try_running_stopped_fsm)
{
  enum states { SINGLE };
  microfsm::fsm<states> fsm{ SINGLE, { SINGLE } };

  EXPECT_EQ(fsm.meta_state(), microfsm::ENDED);

  // Try running
  auto meta_state = fsm.progress();

  ASSERT_EQ(meta_state, microfsm::ENDED);
  ASSERT_EQ(fsm.current_state(), SINGLE);
  ASSERT_EQ(fsm.error_condition(), microfsm::NONE);
}
