#include <gtest/gtest.h>

#include <microfsm/microfsm.hpp>

TEST(state_types, create_with_enum_state)
{
  enum states { FIRST, SECOND };
  microfsm::fsm<states> fsm{ FIRST, { SECOND } };

  ASSERT_EQ(fsm.current_state(), FIRST);
  ASSERT_EQ(fsm.meta_state(), microfsm::RUNNING);
  ASSERT_EQ(fsm.error_condition(), microfsm::NONE);
}

TEST(state_types, create_with_int_state)
{
  microfsm::fsm<int> fsm{ 42, { 123 } };

  ASSERT_EQ(fsm.current_state(), 42);
  ASSERT_EQ(fsm.meta_state(), microfsm::RUNNING);
  ASSERT_EQ(fsm.error_condition(), microfsm::NONE);
}

TEST(state_types, create_with_complex_state)
{
  struct state {
    int x;

    // Need the operator for FSM
    bool operator<(state const & other) const
    {
      return x < other.x;
    }

    // Need the operator for the assert below
    bool operator==(state const & other) const
    {
      return x == other.x;
    }
  };

  microfsm::fsm<state> fsm{ state{ 42 }, { state{ 123 } } };

  ASSERT_EQ(fsm.current_state(), state{ 42 });
  ASSERT_EQ(fsm.meta_state(), microfsm::RUNNING);
  ASSERT_EQ(fsm.error_condition(), microfsm::NONE);
}
